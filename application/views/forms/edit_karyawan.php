<?php
foreach ($detail_karyawan as $data) {
	$nik  = $data->nik;
	$nama_lengkap  = $data->nama_lengkap;
	$tempat_lahir  = $data->tempat_lahir;
	$tgl_lahir  = $data->tgl_lahir;
	$jenis_kelamin  = $data->jenis_kelamin;
	$alamat  = $data->alamat;
	$telp  = $data->telp;
	$kode_jabatan  = $data->kode_jabatan;
	$photo  = $data->photo;
}
     //pisah tanggal bualn tahun
	 $thn_pisah = substr($tgl_lahir, 0, 4);
	 $bln_pisah = substr($tgl_lahir, 5, 2);
	 $tgl_pisah = substr($tgl_lahir, 8, 2);

	?>
<table width="50%" height="78" border="1" align="center">
<tr>
 
  <td colspan="5" align="center" bgcolor="#33FFCC"><b>EDIT KARYAWAN</td></b>
 
 
  
  </tr>
   <div style="color: red" align="center"><?= validation_errors(); ?></div>
<form action="<?=base_url()?>karyawan/editkaryawan/<?= $nik; ?>" method="POST" enctype="multipart/form-data">
<table width="50%" border="0" cellspacing="0" cellpadding="5" bgcolor="#999999" align="center">
  <tr>
    <br />  
    <td>nik</td>
    <td>:</td>
    <td>
      <input value="<?= $nik; ?>" type="text" name="nik" id="nik" readonly></td>
  </tr>
  <tr>
    <td>nama</td>
    <td>:</td>
    <td>
      <input value="<?= $nama_lengkap; ?>" type="text" name="nama_lengkap" id="nama_lengkap" maxlength="50"></td>
  </tr>
  <tr>
    <td>tempat lahir</td>
    <td>:</td>
    <td> <input value="<?= $tempat_lahir; ?>" type="text" name="tempat_lahir" id="tempat_lahir" maxlength="50"></td>
    </td>
  </tr>
   <tr>
    <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
  <link rel="stylesheet" href="/resources/demos/style.css">
  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <td>tanggal lahir</td>
    <td>:</td>
  
    <td>
        <script>
  $( function() {
    $( "#tgl_lahir" ).datepicker({dateFormat: "yy-mm-dd"});
   
  } );
  </script>
      <p><input  value="<?=$tgl_lahir;?>" type="text" name="tgl_lahir" id="tgl_lahir" value="<?=set_value('tgl_lahir');?>"></p>
    </td>
  </tr>
  <tr>
    <td height="35">jenis kelamin</td>
    <td>:</td>
    <td><select name="jenis_kelamin" id="jenis_kelamin">
       <?php
	if($jk == 'P'){
		$slc_p = 'selected';
		$slc_l = '';
	}else if($jk == 'L'){
		$slc_l = 'selected';
		$slc_p = '';
	}else{
		$slc_p = '';
		$slc_l = '';
		}
	?>
     <option <?=$slc_p;?> value="p">perempuan</option>
      <option  <?=$slc_l;?> value="l">laki-laki</option>
      
    </select>
    </td>
  </tr>
   <tr>
    <td>alamat</td>
    <td>:</td>
    <td><textarea name="alamat" id="alamat" cols="45" rows="5"  >"<?= $alamat; ?>"</textarea></td>
  </tr>
 
  <tr>
    <td>telepon</td>
    <td>:</td>
    <td><input value="<?= $telp; ?>" type="text" name="telp" id="telp"></td>
    </td>
  </tr>
 
 
 <tr>
    <td height="35">jabatan</td>
    <td>:</td>
    <td><select name="kode_jabatan" id="kode_jabatan">
    <?php foreach($data_jabatan as $data) {
		      $select_jabatan = ($data->kode_jabatan ==
			  $kode_jabatan) ? 'selected' : '';
		?>
       <option value="<?= $data->kode_jabatan;?>" <?=$select_jabatan; ?>>
       <?= $data->nama_jabatan; ?></option>
       
      
      <?php }?>
      
    </select>
    </td>
  </tr>
  <tr>
    <td style="text-align:left;">upload photo</td>
    <td>:</td>
    <td style="text-align:left;">
    <input type="file" name="image" id="image">
      <input type="hidden" name="foto_old" id="foto_old" value="<?= $photo; ?>">
    </td>
  </tr>
  <tr>
    <td>&nbsp;</td>
    <td>&nbsp;</td>
    <td><input type="submit" name="simpan" id="simpan" value="simpan">
      <input type="submit" name="batal" id="batal" value="reset">
      <br></br>
      <a href="<?=base_url();?>karyawan/listkaryawan"><input type="button" name="kembali ke menu sebelumnya" id="kembali ke menu sebelumnya" value="kembali ke menu sebelumnya"></a></td>
  </tr>
</table>
</table>
</form>
