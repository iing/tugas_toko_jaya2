<style>
.center {
  text-align: center;
}

.pagination {
  display: inline-block;
}

.pagination a {
  color: red;
  float: left;
  padding: 8px 16px;
  text-decoration: none;
  transition: background-color .3s;
  border: 1px solid green;
  margin: 0 4px;
}

.pagination a.active {
  background-color: #4CAF50;
  color: white;
  border: 1px solid #4CAF50;
}
.pagination a:hover:not(.active) {background-color: black;}
</style>
<table width="100%" height="40" border="0" align="center">
<tr>
 
  <td colspan="5" align="center" bgcolor="white"><b><h1><i>"Data barang"</b></h1></i></td>
 
  
  
  </tr>
  </table>
  <?php
  if ($this->session->flashdata('info') == true) {
	  echo $this->session->flashdata('info');
  }
  ?>
<table width="100%" border="1" cellspacing="0" cellpadding="10">
 
    <tr align="center" bgcolor="green">
	<a href="<?=base_url();?>barang/inputbarang"><h2>[New input barang]</h2></a>


 <form action="<?=base_url()?>barang/listbarang" method="POST">
 <td colspan="8" align="right"><label for="Cari Nama"></label>
  <input type="text" name="caridata" id="Cari data" autocomplete="off" placeholder="Cari Nama">
  <input name="tombol_cari" type="submit" id="Cari data" value="Cari data">
  
  </tr>
 
  </td>
  <tr align="center" style="color:#FFF" bgcolor="#000000">
   
    <td width="105"><b>kode Barang</b></td>
    <td width="109"><b>Nama barang</b></td>
    <td width="108"><b>harga barang</b></td>
	<td width="108"><b>nama jenis</B></td>
    <td width="108"><b>stok</B></td>
  
    <td width="108"><b>aksi</B></td>
  </tr>
  <?php
   $data_posisi = $this->uri->segment(4);
  $no = $data_posisi;
  if (count($data_barang) > 0) {
    foreach ($data_barang as $data) {
	
?>
  <tr align="center"  style="color:#993300" bgcolor="mintcream">
 
    <td><b><?= $data->kode_barang; ?></b></td>
    <td><b><?= $data->nama_barang; ?></b></td>
    <td><b><?= $data->harga_barang; ?></b></td>
	<td><b><?= $data->nama_jenis; ?></b></td>
  <td><b><?= $data->stok; ?></b></td>

    
    
     <td><a href="<?=base_url();?>barang/detailbarang/<?= $data->kode_barang; ?>">Detail</a>|
	 <a href="<?=base_url();?>barang/editbarang/<?= $data->kode_barang; ?>">Edit</a>
	 <a href="<?=base_url();?>barang/deletebarang/<?= $data->kode_barang; ?>" onClick="return confirm('Yakin ingin hapus data?');">delete</a> 
	 </td>
  </tr>
  <?php } ?>
   <tr align="center" bgcolor="mintcream">
  <td colspan="7"><b>Halaman:</b> <?= $this->pagination->create_links();?></td>
  </tr>
  <?php
  } else {
    ?>
     <tr align="center">
  <td colspan="7">--tidak ada data -------</td>
  </tr> 
  <?php } ?>
</form>
</table>